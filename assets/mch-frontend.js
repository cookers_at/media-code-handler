(function($) {
    let $container;
    $(document).ready(function () {
        $container = $(mch_wp_object.container);

        $container.find(".input .query").on("click", loadCode);
    });


    function loadCode() {
        $container.find(".input .query").prop( "disabled", true );
        let code = $container.find(".input .code");
        code.prop( "disabled", true );
        $container.find(".loading-spinner").addClass("running");

        $.ajax({
            type: 'POST',
            url: mch_wp_object.ajaxurl,
            data: {
                action: 'ckrs_mch_query_code',
                code: code.val(),
            },
            success: function (data, textStatus, XMLHttpRequest) {
                data = JSON.parse(data);

                $container.find(".output .content").html(data.content);

                if(data.href) {
                    $container.find(".output .download a").attr("href", data.href);
                    $container.find(".output .download").show();
                }

                $container.find(".loading-spinner").removeClass("running");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $container.find(".output .content").html(textStatus);
                $container.find(".loading-spinner").removeClass("running");
            }
        });
    }

}(jQuery));