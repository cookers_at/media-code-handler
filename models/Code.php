<?php


namespace at\cookers\wp\mch;


class Code {
    private $code;
    private $target_id;
    private $used;
    private $valid_until;
    private $claimed_by;
    private $claimed_on;


    /**
     * Code constructor (with or without ARRAY_A input data from DB).
     * @param null $data : the input in ARRAY_A form, if available.
     */
    public function __construct($data = null) {
        if ($data) {
            $this->code = $data["code"];
            $this->target_id = $data["target_id"];
            $this->used = (int) $data["used"];
            $this->valid_until = $data["valid_until"];
            $this->claimed_by = $data["claimed_by"];
            $this->claimed_on = $data["claimed_on"];
        }
    }


    /**
     * @return mixed
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return Code
     */
    public function setCode($code) {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTargetId() {
        return $this->target_id;
    }

    /**
     * @param mixed $target_id
     * @return Code
     */
    public function setTargetId($target_id) {
        $this->target_id = $target_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsed() {
        return $this->used;
    }

    /**
     * @param mixed $used
     * @return Code
     */
    public function setUsed($used) {
        $this->used = $used;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidUntil() {
        return $this->valid_until;
    }

    /**
     * @param mixed $valid_until
     * @return Code
     */
    public function setValidUntil($valid_until) {
        $this->valid_until = $valid_until;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClaimedBy() {
        return $this->claimed_by;
    }

    /**
     * @param mixed $claimed_by
     * @return Code
     */
    public function setClaimedBy($claimed_by) {
        $this->claimed_by = $claimed_by;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClaimedOn() {
        return $this->claimed_on;
    }

    /**
     * @param mixed $claimed_on
     * @return Code
     */
    public function setClaimedOn($claimed_on) {
        $this->claimed_on = $claimed_on;
        return $this;
    }


    /**
     * Checks if the code is currently valid.
     *
     * @return bool: current validity.
     */
    public function isValid() {
        if ($this->code === NULL) return false;

        if ($this->used === 0) {
            return true;
        } else {
            if ($this->valid_until !== NULL && $this->valid_until >= date("Y-m-d H:i:s")) {
                return true;
            }
        }

        return false;
    }


    public function use() {
        if(!is_numeric($this->used)) $this->used = 0;
        $this->used++;

        if($this->claimed_on === NULL) $this->claimed_on = date("Y-m-d H:i:s");
    }


    /**
     * Set the valid_until date to be in $hours hours.
     * @param int $hours
     */
    public function setValidUntilHours($hours) {
        $new_time = date("Y-m-d H:i:s", strtotime("+$hours hours"));
        $this->setValidUntil($new_time);
    }

    public function wasValid() {
        return $this->valid_until !== NULL && $this->valid_until < date("Y-m-d H:i:s");
    }
}