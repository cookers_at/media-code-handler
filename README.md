# README #

 * Plugin Name: Media Code Handler
 * Plugin URI: https://cookers.at/development/media-code-handler/
 * Description: Handles custom codes for secure media distribution.
 * Author: Gerhard Kocher
 * Author URI: https://cookers.at