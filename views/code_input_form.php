<div id="media-code-handler" class="input">
    <div class="input">
        <label for="code">Download-Code:</label>
        <input type="text" class="code" name="code" />
        <button class="query">Absenden</button>
        <div class="center">
            <div class="loading-spinner"><div></div><div></div><div></div><div></div></div>
        </div>
    </div>
    <div class="output">
        <div class="content"><?= $output ?></div>
        <div class="download" style="display: none;">
            <a href="" title="" target="_blank">Datei herunterladen</a>
        </div>
    </div>
</div>