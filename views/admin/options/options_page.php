<div>
    <h2>Einstellungen für Medien-Codes</h2>
    <form method="post" action="options.php">
        <?php settings_fields( $this->option_group ); ?>

        <br/><hr/><br/><!-- --------------------------------------------------- -->

        <table>
            <tr>
                <td><label for="ckrs_mch_case_sensitive">Code Groß/Kleinschreibung</label></td>
                <td>
                    <select id="ckrs_mch_case_sensitive" name="ckrs_mch_case_sensitive">
<!--                        <option value="1" --><?php if(get_option("ckrs_mch_case_sensitive") == "1") echo "selected"; ?><!-- >schreibungsabhängig verwenden</option>-->
                        <option value="0" <?php if(get_option("ckrs_mch_case_sensitive") == "0") echo "selected"; ?> >ignorieren<!-- (alles in Großbuchstaben)--></option>
                    </select>
                    <small>Hinweis: bei aktuellen Datenbankeinstellungen kann nicht zwischen Groß- und Kleinschreibung unterschieden werden.</small>
                </td>
<!--                <td><small>Hinweis: Code wird NUR bei Kunden-Eingabe (wenn ausgewählt) verändert!</small></td>-->
                    <td></td>
            </tr>
            <tr>
                <td><label for="ckrs_mch_message_invalid">Nachricht "Code ungültig"</label></td>
                <td>
                    <?php wp_editor(
                        get_option("ckrs_mch_message_invalid"),
                        "ckrs_mch_message_invalid",
                        [
                            "textarea_name" => "ckrs_mch_message_invalid",
                            "textarea_rows" => 5,
                            "media_buttons" => false,
                        ] ); ?>
                </td>
            </tr>
            <tr>
                <td><label for="ckrs_mch_message_used">Nachricht "Code bereits verwendet"</label></td>
                <td>
                    <?php wp_editor(
                        get_option("ckrs_mch_message_used"),
                        "ckrs_mch_message_used",
                        [
                            "textarea_name" => "ckrs_mch_message_used",
                            "textarea_rows" => 5,
                            "media_buttons" => false,
                        ] ); ?>
                </td>
            </tr>
        </table>

        <?php submit_button(); ?>
    </form>
</div>