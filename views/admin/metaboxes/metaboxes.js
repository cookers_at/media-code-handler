(function($) {

    $(document).ready(function () {
        $("#add_codes .add").on("click", function(event) {
            event.preventDefault();

            if($("body").hasClass("post-new-php")) {
                alert("Please save before adding codes!");
                return false;
            }

            $("#add_codes .output").html("work work...");

            $.ajax({
                type: 'POST',
                url: ckrs_mch_ajax.ajaxurl,
                data: {
                    action: 'ckrs_mch_add_codes',
                    post_id: ckrs_mch_ajax.post_id,
                    codes: $("#add_codes .input").val()
                },
                success: function (data, textStatus, XMLHttpRequest) {
                    $("#add_codes .output").html(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#add_codes .output").html(textStatus);
                }
            });

            return false;

        });
    });

}(jQuery));