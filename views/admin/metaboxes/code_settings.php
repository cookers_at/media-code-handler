<div class="ckrs-metabox">

    <section>
        <aside>
            <label for="download_file">
                Mediendatei für Download
            </label>
        </aside>
        <main>
            <input type="text" name="download_file" value="<?= $download_file ?>">
        </main>
    </section>

    <section>
        <aside>
            <label for="available_hours">
                Verfügbarkeit <small>(in Stunden)</small>
            </label>
        </aside>
        <main>
            <input type="text" name="available_hours" value="<?= $available_hours ?>">
        </main>
    </section>

</div>