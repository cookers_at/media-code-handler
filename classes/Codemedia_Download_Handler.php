<?php


namespace at\cookers\wp\mch;


use Couchbase\Document;

class Codemedia_Download_Handler {
    public function __construct() {
        add_action('wp_ajax_ckrs_mch_query_code', [$this, 'ajax_load_code']);
        add_action('wp_ajax_nopriv_ckrs_mch_query_code', [$this, 'ajax_load_code']);
        add_action('init', [$this, 'file_download_url']);
    }


    /**
     * Direct download access handler (via GET).
     */
    function file_download_url() {
        if (isset($_GET['code']) && isset($_GET['download'])) {
            $dao = new Code_DAO();
            $code = $dao->get_code($_GET["code"]);

            sleep(2); // as a security feature

            if(!$code->isValid()) {
                header("X-Error: Code not valid.");
                die("An error occured.");
            }

            $file = get_post_meta($code->getTargetId(), 'download_file', true);

            $code->use();
            $code->setValidUntilHours(get_post_meta($code->getTargetId(), 'available_hours', true));

            $dao->update_code($code);

            $download = new Download($file);
            $download->serveFileSecure();
        }
    }


    public function ajax_load_code() {
        if (isset($_POST['code'])) {
            $dao = new Code_DAO();
            $code = $dao->get_code($_POST["code"]);

            sleep(2); // as a security feature

            if(!$code->isValid()) {
                if($code->wasValid()) {
                    echo json_encode(["class" => "found", "content" => get_option("ckrs_mch_message_used")]);
                    die();
                } else {
                    echo json_encode(["class" => "error", "content" => get_option("ckrs_mch_message_invalid")]);
                    die();
                }
            }


            $post_content = get_post($code->getTargetId());
            $content = $post_content->post_content;
            $content = apply_filters('the_content', $content);

            $return = [
                "class" => "found",
                "href" => get_site_url() . "?download&code=" . $code->getCode(),
                "content" => $content
            ];
            echo json_encode($return);
        }

        die();
    }
}