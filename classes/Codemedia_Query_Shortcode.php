<?php


namespace at\cookers\wp\mch;


class Codemedia_Query_Shortcode {
    /**
     * Handler for the shortcode [media-code-field]
     *
     * @param array $atts
     * @param null $output
     * @return string: the content.
     */
    public function shortcode_frontend_query($atts = [], $output = null) {
        wp_enqueue_script('ckrs-mch-frontend', CKRS_MCH_URL . 'assets/mch-frontend.js', array('jquery'), "0.1.1", true);
        wp_enqueue_style( 'ckrs-mch-styles', CKRS_MCH_URL . 'views/code_input_form.css', [], "0.1.2" );

        wp_localize_script('ckrs-mch-frontend', 'mch_wp_object',
            array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) , 'container' => "#media-code-handler", 'random' => "/lTV6Ofx" ) );

        ob_start();
        require( CKRS_MCH_DIR . 'views/code_input_form.php' );
        $content = ob_get_clean();

        return $content;
    }
}