<?php


namespace at\cookers\wp\mch;


if (!class_exists(__NAMESPACE__ . '\Setup')) {

    class Setup {

        /**
         *
         * @access   private
         * @var      Setup $instance Instance of this class.
         */
        private static $instance;

        /**
         * Provides access to a single instance of a module using the singleton pattern
         *
         * @return object
         */
        public static function get_instance() {

            if (null === self::$instance) {
                self::$instance = new self();
            }

            return self::$instance;

        }

        /**
         * Constructor
         */
        protected function __construct() {

            $this->load_libraries();
            spl_autoload_register([$this, 'load_dependencies']);

            $codemedia_post_type = new Codemedia_Post_Type();
            $codemedia_post_type->add_wp_actions();

            new Codemedia_Options();

            new Codemedia_Download_Handler();

            add_shortcode('media-code-field', [new Codemedia_Query_Shortcode(), 'shortcode_frontend_query']);
        }

        /**
         * Loads libraries.
         *
         * - [none]
         */
        private function load_libraries() {

        }

        /**
         * Loads all Plugin dependencies
         */
        private function load_dependencies($class) {
            if (false !== strpos($class, __NAMESPACE__)) {
                $class = str_replace(__NAMESPACE__ . '\\', '', $class);

                //  $class_file_name = str_replace('_', '-', strtolower($class)) . '.php';
                $class_file_name = $class . '.php';
                $folder = '/';

                if (false !== strpos($class, '_Admin')) {
                    $folder .= 'admin/';
                }

                if (false !== strpos($class, 'Controller')) {
                    $path = CKRS_MCH_DIR . 'controllers' . $folder . $class_file_name;
                    require_once($path);
				} elseif ( file_exists( CKRS_MCH_DIR . 'models' . $folder . $class_file_name ) ) {
					$path = CKRS_MCH_DIR . 'models' . $folder . $class_file_name;
					require_once( $path );
                } else {
                    $path = CKRS_MCH_DIR . 'classes/' . $class_file_name;
                    require_once($path);
                }
            }
        }


        /**
         * Prepares the activation hook.
         */
        public function register_activation_hook($file) {
            register_activation_hook($file, [$this, 'activate']);
        }


        /**
         * Prepares sites to use the plugin during single or network-wide activation.
         */
        public function activate() {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $code_dao = new Code_DAO();
            $sql = $code_dao->create_table();

            dbDelta($sql);
        }

        /**
         * Rolls back activation procedures when de-activating the plugin
         */
        public function deactivate() {

            return true;

        }

        /**
         * Fired when user uninstalls the plugin, called in uninstall.php file
         */
        public static function uninstall_plugin() {

            return true;

        }

    }

}