<?php


namespace at\cookers\wp\mch;


class Download {
    private $file;
    private $file_basename;
    private $content_type;

    public function __construct($file) {
        $this->file = $file;
        $this->file_basename = basename($file);
        $this->content_type = function_exists('mime_content_type') ? mime_content_type($this->file) : wp_check_filetype($this->file)['type'];
    }


    private static function is_url( $url ) {
        $result = ( false !== filter_var( $url, FILTER_VALIDATE_URL ) );
        return apply_filters( '__is_url', $result, $url );
    }


    private function checkDir() {
        if (headers_sent($_filename, $_linenum)) {
            die("Headers already sent in $_filename on line $_linenum");
        }

        if (substr_count($this->file, "../") > 0) {
            die("Please, no funny business, however, nice try though!");
        }

//        if (self::is_url($this->file_dir)) {
//            header("location: " . $this->file_dir);
//            die();
//        }
    }


    public function serveFileSecure() {
        if (function_exists('ini_set'))
            @ini_set('display_errors', 0);

        @session_write_close();

        if (function_exists('apache_setenv'))
            @apache_setenv('no-gzip', 1);

        if (function_exists('ini_set'))
            @ini_set('zlib.output_compression', 'Off');


        @set_time_limit(0);
        @session_cache_limiter('none');


        nocache_headers();
        header("X-Robots-Tag: noindex, nofollow", true);
        header("Robots: none");
        header('Content-Description: File Transfer');


        $file = fopen($this->file, 'rb');

        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename={$this->file_basename}");
        header("Content-Length: " . filesize($this->file));
        fpassthru($file);

        exit;
    }
}