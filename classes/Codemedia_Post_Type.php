<?php


namespace at\cookers\wp\mch;


class Codemedia_Post_Type {
    public function add_wp_actions() {
        add_action('init', [$this, 'add_post_type']);
        add_action('admin_init', [$this, 'add_post_meta_boxes']);
        add_action('save_post', [$this, 'save_settings_meta_box'], 1, 2);
		add_action('admin_enqueue_scripts', [$this, 'add_admin_scripts']);
        add_action('wp_ajax_ckrs_mch_add_codes', [$this, 'ajax_add_codes'] );
    }


    /**
     * Add the custom post type.
     */
    public function add_post_type() {

        $labels = array(
            'name' => __('Medien-Code', CKRS_MCH_I18N_DOMAIN),
            'singular_name' => __('Medien-Code', CKRS_MCH_I18N_DOMAIN),
            'add_new' => __('Neuer Medien-Code', CKRS_MCH_I18N_DOMAIN),
            'add_new_item' => __('Neuer Medien-Code', CKRS_MCH_I18N_DOMAIN),
            'edit_item' => __('Medien-Code ändern', CKRS_MCH_I18N_DOMAIN),
            'new_item' => __('Neuer Medien-Code', CKRS_MCH_I18N_DOMAIN),
            'view_item' => __('Medien-Code ansehen', CKRS_MCH_I18N_DOMAIN),
            'search_items' => __('Medien-Code suchen', CKRS_MCH_I18N_DOMAIN),
            'not_found' => __('Keine Medien-Code gefunden', CKRS_MCH_I18N_DOMAIN),
            'not_found_in_trash' => __('Keine Medien-Code im Papierkorb', CKRS_MCH_I18N_DOMAIN)
        );
        $supports = array(
            'editor',
            'title',
//			'revisions'
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
//			'rewrite'             => [ 'slug' => 'qrcode' ],
//			'show_in_rest'        => false, // Gutenberg
            'public' => true,
            'has_archive' => true,
            'exclude_from_search' => true,
            'show_in_nav_menus' => false,
            'menu_position' => 40,
            'menu_icon' => 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBhcmlhLWhpZGRlbj0idHJ1ZSIgZm9jdXNhYmxlPSJmYWxzZSIgd2lkdGg9IjFlbSIgaGVpZ2h0PSIxZW0iIHN0eWxlPSItbXMtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTsgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpOyB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpOyIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQgbWVldCIgdmlld0JveD0iMCAwIDIwIDIwIj48cGF0aCBkPSJNMTcgNEgzYy0xLjEgMC0yIC45LTIgMnY4YzAgMS4xLjkgMiAyIDJoMTRjMS4xIDAgMi0uOSAyLTJWNmMwLTEuMS0uOS0yLTItMnptLTcgM0g3djRjMCAxLjEtLjkgMi0yIDJzLTItLjktMi0ycy45LTIgMi0yYy40IDAgLjcuMSAxIC4zVjVoNHYyem00IDMuNUwxMi41IDEybDEuNSAxLjVWMTVsLTMtM2wzLTN2MS41em0xIDQuNXYtMS41bDEuNS0xLjVsLTEuNS0xLjVWOWwzIDNsLTMgM3oiIGZpbGw9IiM2MjYyNjIiLz48L3N2Zz4=',
        );
        register_post_type(CKRS_MCH_CODEMEDIA_POSTTYPE, $args);

    }


    /**
     * Add the post meta boxes
     *
     * see https://developer.wordpress.org/reference/functions/add_meta_box for a full explanation of each property
     */
    function add_post_meta_boxes() {
        add_meta_box(
            "used_codes", // div id containing rendered fields
            __('Eingelöste Codes', CKRS_MCH_I18N_DOMAIN), // section heading displayed as text
            [$this, "meta_box_list_used"], // callback function to render fields
            CKRS_MCH_CODEMEDIA_POSTTYPE, // name of post type on which to render fields
            "normal", // location on the screen
            "low" // placement priority
        );

        add_meta_box(
            "open_codes", // div id containing rendered fields
            __('Offene/ungebrauchte Codes', CKRS_MCH_I18N_DOMAIN), // section heading displayed as text
            [$this, "meta_box_list_open"], // callback function to render fields
            CKRS_MCH_CODEMEDIA_POSTTYPE, // name of post type on which to render fields
            "normal", // location on the screen
            "low" // placement priority
        );

        add_meta_box(
            "add_codes", // div id containing rendered fields
            __('Codes hinzufügen', CKRS_MCH_I18N_DOMAIN), // section heading displayed as text
            [$this, "meta_box_add_codes"], // callback function to render fields
            CKRS_MCH_CODEMEDIA_POSTTYPE, // name of post type on which to render fields
            "side", // location on the screen
            "low" // placement priority
        );

        add_meta_box(
            "settings", // div id containing rendered fields
            __('Einstellungen', CKRS_MCH_I18N_DOMAIN), // section heading displayed as text
            [$this, "meta_box_settings"], // callback function to render fields
            CKRS_MCH_CODEMEDIA_POSTTYPE, // name of post type on which to render fields
            "side", // location on the screen
            "default" // placement priority
        );
    }


    /**
     * Display the used-code metabox (simple list).
     */
    public function meta_box_list_used() {
        global $post;
        $codes = (new Code_DAO())->get_used_codes($post->ID);

        $codes = array_map([$this, 'filter_code_string'], $codes);

        echo sizeof($codes) . " Codes: " . join(", ", $codes);
    }
    private function filter_code_string($c) {
        // TODO: getClaimedBy
        return "<code title=\"Eingelöst: {$c->getClaimedOn()}\">{$c->getCode()}</code>";
    }


    /**
     * Display the open/unused-code metabox.
     */
    public function meta_box_list_open() {
        global $post;
        $codes = (new Code_DAO())->get_open_codes($post->ID);

        $codes = array_map([$this, 'filter_code_with_claimed'], $codes);

        echo sizeof($codes) . " Codes: " . join(", ", $codes);
    }
    private function filter_code_with_claimed($c) {
        return "<code>{$c->getCode()}</code>";
    }


    /**
     * Display the form for adding codes to this post.
     */
    public function meta_box_add_codes() {
        require( CKRS_MCH_DIR . 'views/admin/metaboxes/add_codes.php' );
    }


    /**
     * AJAX handler for adding codes via the metabox.
     */
    public function ajax_add_codes() {
        $dao = new Code_DAO();
        $codes = explode("\n", $_POST['codes']);
        $count = 0;
        $not_added = [];

        if(!isset($_POST['post_id']) || CKRS_MCH_CODEMEDIA_POSTTYPE !== get_post_type($_POST['post_id'])) {
            die("Nichts hinzugefügt! (post_id error)");
        }

        foreach ($codes as $code) {
            $code = trim($code);

            $added = $dao->add_code($code, $_POST['post_id']);

            if($added) {
                $count++;
            } else {
                $not_added[] = $code;
            }
        }

        echo "$count Codes hinzugefügt.";

        if(!empty($not_added)) {
            $skipped = count($not_added);
            echo " (Übersprungen: $skipped)";
        }

        die();
    }


    /**
     * Display the settings metabox from view.
     *
     * @param $post
     */
    function meta_box_settings( $post ) {
        $download_file = get_post_meta( $post->ID, 'download_file', true );
        $available_hours = get_post_meta( $post->ID, 'available_hours', true );

        wp_nonce_field( basename( __FILE__ ), 'ckrs_mch_nonce' );

        require( CKRS_MCH_DIR . 'views/admin/metaboxes/code_settings.php' );
    }


    /**
     * Save the settings from the metabox to post_meta.
     *
     * @param $post_id
     * @return string on error.
     */
    function save_settings_meta_box($post_id) {
        // check autosave
        if (wp_is_post_autosave($post_id)) {
            return 'autosave';
        }

        //check post revision
        if (wp_is_post_revision($post_id)) {
            return 'revision';
        }

        // check permissions
        if (CKRS_MCH_CODEMEDIA_POSTTYPE == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return 'cannot edit page';
            }
        } elseif (!current_user_can('edit_post', $post_id)) {
            return 'cannot edit post';
        }

        // check nonce
        if (!isset($_POST['ckrs_mch_nonce']) || !wp_verify_nonce($_POST['ckrs_mch_nonce'], basename(__FILE__))) {
            return 'nonce not verified';
        }

        // finally save stuff
        update_post_meta($post_id, 'download_file', $_POST["download_file"]);
        update_post_meta($post_id, 'available_hours', $_POST["available_hours"]);
    }


    /**
     * Enqueue scripts and styles if on the CPT page.
     *
     * @param $hook_suffix
     */
    function add_admin_scripts( $hook_suffix ){
        if( in_array($hook_suffix, array('post.php', 'post-new.php') ) ){
            $screen = get_current_screen();

            if( is_object( $screen ) && CKRS_MCH_CODEMEDIA_POSTTYPE == $screen->post_type ){
                global $post;

                wp_enqueue_script( 'ckrs-mch-code-backend-script', CKRS_MCH_URL . 'views/admin/metaboxes/metaboxes.js', ["jquery"], rand(0,999) );
                wp_localize_script('ckrs-mch-code-backend-script', 'ckrs_mch_ajax',
                    array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'post_id' => $post->ID ) );

                wp_enqueue_style( 'ckrs-mch-styles', CKRS_MCH_URL . 'views/admin/metaboxes/style.css' );
            }
        }
    }
}