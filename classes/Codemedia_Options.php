<?php


namespace at\cookers\wp\mch;


class Codemedia_Options {
	private $option_group = CKRS_MCH_CODEMEDIA_POSTTYPE."-options";


    /**
     * Codemedia_Options constructor to add the actions to WP.
     */
    public function __construct() {
        add_action( 'admin_init', [ $this, 'register_settings' ] );
        add_action( 'admin_menu', [ $this, 'add_options_page' ], 9);
    }

	public function add_options_page() {
		add_submenu_page(
			'edit.php?post_type='.CKRS_MCH_CODEMEDIA_POSTTYPE,
			__( 'Einstellungen für Medien-Code', CKRS_MCH_I18N_DOMAIN ),
			__( 'Einstellungen', CKRS_MCH_I18N_DOMAIN ),
			'manage_options',
			'settings',
			[ $this, 'display_options_page' ] );
	}


	public function display_options_page() {
		require_once( CKRS_MCH_DIR . 'views/admin/options/options_page.php' );
	}


	public function register_settings() {
        register_setting( $this->option_group, "ckrs_mch_case_sensitive");
        register_setting( $this->option_group, "ckrs_mch_message_invalid");
        register_setting( $this->option_group, "ckrs_mch_message_used");
    }
}