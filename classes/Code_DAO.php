<?php


namespace at\cookers\wp\mch;


class Code_DAO {
    const DB_TABLE_NAME = "mch_codes";
    private $table_name;


    /**
     * Static loading endpoint for code.
     *
     * @param string $code: the input code.
     * @return Code: the Code object.
     */
    public static function load($code) {
        $dao = new self();
        return $dao->get_code($code);
    }


    /**
     * Code_DAO constructor.
     */
    public function __construct() {
        global $wpdb;
        $this->table_name = $wpdb->prefix . self::DB_TABLE_NAME;
    }


    /**
     * Generates the SQL code to CREATE the <em>DB_TABLE</em> table.
     *
     * @return string: the SQL CREATE statement.
     */
    public function create_table() {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = <<<SQL
CREATE TABLE `{$this->table_name}` (
	`code` VARCHAR(80) NOT NULL,
	`target_id` BIGINT(20) NOT NULL,
	`used` BOOLEAN NOT NULL DEFAULT FALSE,
	`valid_until` DATETIME DEFAULT NULL,
	`claimed_by` TEXT DEFAULT NULL,
	`claimed_on` DATETIME DEFAULT NULL,
	PRIMARY KEY (`code`)
); $charset_collate;
SQL;

        return $sql;
    }


    /**
     * Add a new code to a target Codemedia_Post.
     *
     * @param string $code
     * @param string|int $target
     * @param string|null $valid_until
     * @return bool: <code>true</code> if successfully inserted.
     *               <code>false</code> otherwise.
     */
    public function add_code($code, $target, $valid_until = NULL) {
        global $wpdb;

        $code = trim($code);

        if(!$code || !$target) return false;

        $success = $wpdb->insert(
            $this->table_name,
            [
                'code' => $code,
                'target_id' => $target,
                'valid_until' => $valid_until
            ]
        );

        return $success === 1;
    }


    /**
     * Get one Code from DB.
     *
     * @param string $code: the code string.
     * @return Code: the Code object to the code string.
     */
    public function get_code($code) {
        global $wpdb;

        $sql = $wpdb->prepare(
            "SELECT * FROM {$this->table_name} where code = \"%s\"",
            $code
        );
        $result = $wpdb->get_row( $sql , ARRAY_A );

        return new Code($result);
    }


    /**
     * Get all used Codes from DB.
     *
     * @param $target_id
     * @return array: the used Code objects.
     */
    public function get_used_codes($target_id) {
        return $this->get_codes_by_used_status($target_id, 1);
    }


    /**
     * Get all open/unused Codes from DB.
     *
     * @param $target_id
     * @return array: the open Code objects.
     */
    public function get_open_codes($target_id) {
        return $this->get_codes_by_used_status($target_id, 0);
    }


    /**
     * Get all Codes from DB that match the $used query.
     *
     * @param $target_id
     * @param bool $used : the status.
     * @return array: the Code objects.
     */
    private function get_codes_by_used_status($target_id, $used) {
        global $wpdb;

        $used = (int) $used; // MySQL needs 0 and 1
        $results = $wpdb->get_results(
            "SELECT * FROM {$this->table_name} WHERE target_id = $target_id AND used = $used ORDER BY claimed_on ASC, code ASC", ARRAY_A
        );

        $codes = [];
        foreach ($results as $result) {
            $codes[] = new Code($result);
        }

        return $codes;
    }


    /**
     * @param Code $code: the code.
     */
    public function update_code($code) {
        global $wpdb;

        $wpdb->update(
            $this->table_name,
            [
                'target_id' => $code->getTargetId(),
                'used' => $code->getUsed(),
                'valid_until' => $code->getValidUntil(),
                'claimed_by' => $code->getClaimedBy(),
                'claimed_on' => $code->getClaimedOn()
            ],
            [
                'code' => $code->getCode()
            ]
        );
    }
}